package main

import (
	"flag"
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/forgems/flowjam/locations"
	"gitlab.com/forgems/flowjam/weather"
)

var listenAddr = flag.String("listen", "0.0.0.0:8000", "server listen address")
var db = flag.String("db", "", "database connection string")
var apiKey = flag.String("apiKey", "", "Open Weather Map api key")

func main() {
	flag.Parse()
	log.Println("DB:", *db)
	r := gin.Default()
	store, err := locations.NewPGStore(*db)
	if err != nil {
		log.Fatal(err)
	}
	handler := locations.LocationHandler{
		Store:   store,
		Weather: weather.NewWeatherService(*apiKey),
	}
	handler.RegisterRoutes(r)
	err = r.Run(*listenAddr) // listen and serve on 0.0.0.0:8080
	if err != nil {
		log.Fatal(err)
	}
}
