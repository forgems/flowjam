BEGIN;
DROP TABLE IF EXISTS location_conditions;
DROP TABLE IF EXISTS queries;
DROP TABLE IF EXISTS locations;

CREATE TABLE locations (
	id serial PRIMARY KEY,
	query text NOT NULL
);

CREATE TABLE queries (
	id serial PRIMARY KEY,
	location_id integer NOT NULL REFERENCES locations(id) ON DELETE CASCADE,
	timestamp timestamp NOT NULL DEFAULT NOW(),
	temp float NOT NULL,
	min_temp float NOT NULL,
	max_temp float NOT NULL
);

CREATE INDEX queries_location_idx ON queries(location_id, timestamp);

CREATE TABLE location_conditions (
	id serial PRIMARY KEY,
	query_id integer NOT NULL REFERENCES queries(id) ON DELETE CASCADE,
	name text NOT NULL
);

CREATE INDEX location_conditions_query_idx ON location_conditions(query_id);

COMMIT;
