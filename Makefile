

build: main.go
	CGO_ENABLED=0 go build -a -tags netgo -ldflags '-s -w' -mod vendor -o flowjam

test: up
	docker-compose exec -T db dropdb -U postgres flowjam_test || true
	docker-compose exec -T db createdb -U postgres flowjam_test
	docker-compose exec -T db psql -U postgres flowjam_test < schema.sql
	go test -v -coverprofile=coverage.out gitlab.com/forgems/flowjam/... -args -apiKey=$(API_KEY)

coverage: test
	go tool cover -func=coverage.out

coverage_browser: test
	go tool cover -html=coverage.out

container:
	docker-compose build app

up:
	docker-compose up -d

down:
	docker-compose down

run:
	go run main.go -apiKey $(API_KEY) -db "dbname=flowjam_test username=postgres password=postgres sslmode=disable host=localhost"

sql:
	docker-compose exec db psql -U postgres flowjam

.PHONY: build test coverage container up down sql coverage_browser
