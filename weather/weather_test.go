package weather

import (
	"flag"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

var apiKey = flag.String("apiKey", "", "Open Weather Map api key")

func TestWeatherService(t *testing.T) {
	svc := NewWeatherService(*apiKey)
	t.Run("Query known location", func(t *testing.T) {
		_, err := svc.CurrentWeather("Warsaw")
		require.NoError(t, err)
	})

	t.Run("Query unknown location", func(t *testing.T) {
		resp, err := svc.CurrentWeather("XYZ")

		require.Error(t, err)
		require.Nil(t, resp)
	})

	t.Run("Query empty location", func(t *testing.T) {
		resp, err := svc.CurrentWeather("")

		require.Error(t, err)
		require.Nil(t, resp)
	})

	t.Run("Temp", func(t *testing.T) {
		data := &WeatherData{}
		require.InDelta(t, data.Temp(), AbsoluteZero, 0.1)
		require.InDelta(t, data.TempMin(), AbsoluteZero, 0.1)
		require.InDelta(t, data.TempMax(), AbsoluteZero, 0.1)
	})
	t.Run("Conditions", func(t *testing.T) {
		data := &WeatherData{}
		require.Len(t, data.Conditions(), 0)
	})
	t.Run("Conditions", func(t *testing.T) {
		data := &WeatherData{
			Weather: []Conditions{
				{
					Main: "test",
				},
			},
		}
		conditions := data.Conditions()
		require.Len(t, conditions, 1)
		require.Equal(t, conditions[0], "test")
	})
}

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}
