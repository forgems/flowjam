package weather

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Service interface {
	CurrentWeather(location string) (*WeatherData, error)
}

const AbsoluteZero = -273.15

type Conditions struct {
	Main        string
	Description string
	Id          int
	Icon        string
}

type WeatherData struct {
	Weather []Conditions

	Main struct {
		Temp     float32
		Pressure float32
		Humidity float32
		TempMin  float32 `json:"temp_min"`
		TempMax  float32 `json:"temp_max"`
	}

	DT int64

	Wind struct {
		Speed float32
		Deg   float32
	}

	Name string
}

func (w *WeatherData) Time() time.Time {
	return time.Unix(w.DT, 0)
}

func (w *WeatherData) Temp() float32 {
	return w.Main.Temp + AbsoluteZero
}

func (w *WeatherData) TempMin() float32 {
	return w.Main.TempMin + AbsoluteZero
}

func (w *WeatherData) TempMax() float32 {
	return w.Main.TempMax + AbsoluteZero
}

func (w *WeatherData) Conditions() []string {
	result := make([]string, len(w.Weather))
	for i := range w.Weather {
		result[i] = w.Weather[i].Main
	}
	return result
}

type invalid struct {
	Message string
}

type service struct {
	apiKey string
}

func NewWeatherService(apiKey string) Service {
	return service{apiKey}
}

func (s service) CurrentWeather(location string) (*WeatherData, error) {
	v := url.Values{}
	v.Add("appid", s.apiKey)
	v.Add("q", string(location))
	resp, err := http.Get("https://api.openweathermap.org/data/2.5/weather?" + v.Encode())

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	if !strings.HasPrefix(resp.Header.Get("content-type"), "application/json") {
		buf, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("invalid response: %s", buf)

	}

	decoder := json.NewDecoder(resp.Body)

	if resp.StatusCode != http.StatusOK {
		result := invalid{}
		err := decoder.Decode(&result)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("invalid request:%d %s", resp.StatusCode, result.Message)
	}
	data := WeatherData{}
	err = decoder.Decode(&data)
	if err != nil {
		return nil, err
	}
	return &data, nil
}
