FROM golang:1.12 AS builder
RUN mkdir /work
COPY . /work/
WORKDIR /work
RUN make build

FROM scratch
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /work/flowjam /flowjam
ENTRYPOINT ["/flowjam"]
