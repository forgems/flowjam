Weather API
-----------

Challenge
=========
Write a RESTful web service using the *Weather Map API* to retrieve weather
data.

Product Requirements:

1. Allows the user to bookmark places that they want to keep tabs on
2. Can send a query for a location and get back the weather for that day
3. Keeps track of the list of queries that has been entered
4. Based on the queries, returns basic statistics such as:
  * the total number of queries
  * days it was cloudy, drizzled, etc
  * average temperature by month
  * highs for each month
  * lows for each month

Technical Requirements

* README.md documenting set up
* Use a SQL database
* Write unit tests
* Bonus: Docker setup

Guiding Questions
* What endpoints will you have? How many endpoints will you have?

Running
=======
In order to run this project you have to obtain the API key for Open Weather Map end set it as environment variable
```
export API_KEY=xxxxxxxxxxxxxxxxxx
```
After that the app can be run with docker-compose by issuing following command
```
docker-compose up
```
or
```
make up
```

API
===

The application exposes the following endpoints

/api/locations [GET, POST]
--------------
POST - create a new bookmark
```http
http POST http://localhost:8000/api/locations/ query=Bialystok
HTTP/1.1 200 OK
Content-Length: 28
Content-Type: application/json; charset=utf-8
Date: Sun, 31 Mar 2019 22:54:21 GMT

{
    "ID": 2,
    "Query": "Bialystok"
}
```
GET - get a list of bookmarks
```http
http GET http://localhost:8000/api/locations/
HTTP/1.1 200 OK
Content-Length: 58
Content-Type: application/json; charset=utf-8
Date: Sun, 31 Mar 2019 22:55:14 GMT

[
    {
        "ID": 1,
        "Query": "Warszawa"
    },
    {
        "ID": 2,
        "Query": "Bialystok"
    }
]
```

/api/locations/:id/
------------------
Supported methods are:

GET - fetch current weather conditions for given location
```http
http GET http://localhost:8000/api/locations/1/
HTTP/1.1 200 OK
Content-Length: 284
Content-Type: application/json; charset=utf-8
Date: Sun, 31 Mar 2019 22:59:00 GMT

{
    "CurrentWeather": {
        "DT": 1554072560,
        "Main": {
            "Humidity": 55,
            "Pressure": 1023,
            "Temp": 3.3600159,
            "temp_max": 6.6700134,
            "temp_min": 1.1100159
        },
        "Name": "Warsaw",
        "Weather": [
            {
                "Description": "clear sky",
                "Icon": "01n",
                "Id": 800,
                "Main": "Clear"
            }
        ],
        "Wind": {
            "Deg": 360,
            "Speed": 3.6
        }
    },
    "ID": 1,
    "Query": "Warszawa"
}
```
PUT, PATCH - update current location (onyl query field is supported)
```http
http PATCH http://localhost:8000/api/locations/1/ query=Warsaw
HTTP/1.1 200 OK
Content-Length: 25
Content-Type: application/json; charset=utf-8
Date: Sun, 31 Mar 2019 22:59:51 GMT

{
    "ID": 1,
    "Query": "Warsaw"
}

```
DELETE - delete the bookmark
```http
http DELETE http://localhost:8000/api/locations/1/
HTTP/1.1 204 No Content
Date: Sun, 31 Mar 2019 23:00:24 GMT
```


/api/locations/:id/statistics/
------------------------------
Get usage statistics about location
```http
http GET http://localhost:8000/api/locations/3/statistics/
HTTP/1.1 200 OK
Content-Length: 180
Content-Type: application/json; charset=utf-8
Date: Sun, 31 Mar 2019 23:17:06 GMT

{
    "average_temps_by_month": {
        "2019-03": 2.9500122
    },
    "condition_days": {
        "Clear": 1
    },
    "max_temps_by_month": {
        "2019-03": 5.5599976
    },
    "min_temps_by_month": {
        "2019-03": 0.55999756
    },
    "query_count": 3
}
```
