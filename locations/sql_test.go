package locations

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/forgems/flowjam/weather"
)

func TestSQLStore(t *testing.T) {
	store, err := NewPGStore(*conString)
	require.NoError(t, err)

	t.Run("Save/GET", func(t *testing.T) {
		loc := Location{Query: "Warszawa"}
		err := store.Save(&loc)
		assert.NoError(t, err)
		assert.NotEqual(t, 0, loc.ID)

		savedID := loc.ID
		err = store.Save(&loc)
		assert.NoError(t, err)
		assert.Equal(t, savedID, loc.ID)

		newLoc, err := store.Get(savedID)
		assert.NoError(t, err)
		assert.Equal(t, newLoc.ID, loc.ID)
		assert.Equal(t, newLoc.Query, loc.Query)
	})

	t.Run("List", func(t *testing.T) {
		locations, err := store.List()
		assert.NoError(t, err)
		assert.NotEmpty(t, locations)
		for i := range locations {
			assert.NotEqual(t, 0, locations[i].ID)
			assert.NotEqual(t, "", locations[i].Query)
		}
	})

	t.Run("Get nonexistant", func(t *testing.T) {
		location, err := store.Get(0)
		assert.NoError(t, err)
		assert.Nil(t, location)
	})

	t.Run("Delete", func(t *testing.T) {
		locations, err := store.List()
		assert.NoError(t, err)
		assert.NotEmpty(t, locations)
		err = store.Delete(locations[0])
		assert.NoError(t, err)
	})

	t.Run("Statistics", func(t *testing.T) {
		loc := Location{Query: "Warszawa"}
		err := store.Save(&loc)
		assert.NoError(t, err)
		assert.NotEqual(t, 0, loc.ID)
		now := time.Now()
		data := weather.WeatherData{
			DT: now.Unix(),
			Weather: []weather.Conditions{
				weather.Conditions{
					Main: "cloudy",
				},
				weather.Conditions{
					Main: "sunny",
				},
			},
		}
		err = store.SaveQuery(&loc, &data)
		assert.NoError(t, err)
		fmt.Println(store.GetStatistics(&loc))
	})
}
