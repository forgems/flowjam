package locations

import "gitlab.com/forgems/flowjam/weather"

type Location struct {
	ID    int
	Query string
}

type LocationStatistics struct {
	QueryCount          int64              `json:"query_count"`
	ConditionsDays      map[string]int     `json:"condition_days"`
	AverageTempsByMonth map[string]float32 `json:"average_temps_by_month"`
	MinTempsByMonth     map[string]float32 `json:"min_temps_by_month"`
	MaxTempsByMonth     map[string]float32 `json:"max_temps_by_month"`
}

type Store interface {
	List() ([]*Location, error)
	Get(id int) (*Location, error)
	Save(location *Location) error
	Delete(location *Location) error
	GetStatistics(location *Location) (*LocationStatistics, error)
	SaveQuery(location *Location, data *weather.WeatherData) error
}

/*
type FakeStore map[int]*Location

func (f FakeStore) List() (locations []*Location, err error) {
	for _, loc := range f {
		locations = append(locations, loc)
	}
	return locations, nil
}

func (f FakeStore) Get(id int) (*Location, error) {
	return f[id], nil
}

func (f FakeStore) Save(location *Location) error {
	if location.ID == 0 {
		location.ID = len(f)
	}
	f[location.ID] = location
	return nil
}

func (f FakeStore) Delete(location *Location) error {
	delete(f, location.ID)
	return nil
}

func (f FakeStore) SaveQuery(location *Location, data *weather.WeatherData) error {
	return nil
}

func (f FakeStore) GetStatistics(location *Location) (*LocationStatistics, error) {
	return &LocationStatistics{}, nil
}

var _ Store = FakeStore{}
*/
