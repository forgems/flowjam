package locations

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"

	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/forgems/flowjam/weather"
)

var conString = flag.String("db", "dbname=flowjam_test user=postgres password=postgres sslmode=disable", "database connection string")
var apiKey = flag.String("apiKey", "", "Open Weather Map api key")

func TestAPI(t *testing.T) {
	// DB setup
	router := gin.Default()
	db, err := sqlx.Connect("postgres", *conString)
	assert.NoError(t, err)
	_, err = db.Exec(`
		delete from location_conditions;
		delete from queries;
		delete from locations;
	`)
	assert.NoError(t, err)

	store, _ := NewPGStore(*conString)

	handler := LocationHandler{
		store,
		weather.NewWeatherService(*apiKey),
	}
	handler.RegisterRoutes(router)

	location := Location{Query: "Warszawa"}
	err = store.Save(&location)
	assert.NoError(t, err)
	locationURL := fmt.Sprintf("/api/locations/%d/", location.ID)

	t.Run("test list", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/api/locations/", nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)
		fmt.Println(w.Body.String())
	})

	t.Run("test get current weather", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("GET", locationURL, nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)
		// check statistics
		w = httptest.NewRecorder()
		req = httptest.NewRequest("GET", locationURL+"statistics/", nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)
		//json.Unmarshal(w.Body.Bytes())
	})
	t.Run("test get weather for mising bookmark", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("GET", "/api/locations/0/", nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 404, w.Code)
	})

	t.Run("test update with no data", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("PATCH", locationURL, nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 400, w.Code)
	})

	t.Run("test update with data", func(t *testing.T) {
		data, _ := json.Marshal(map[string]string{"query": "Bialystok"})
		w := httptest.NewRecorder()
		req := httptest.NewRequest("PATCH", locationURL, bytes.NewReader(data))
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)
		fmt.Println(w.Body.String())
	})

	t.Run("test create with data", func(t *testing.T) {
		data, _ := json.Marshal(map[string]string{"query": "Bialystok"})
		w := httptest.NewRecorder()
		req := httptest.NewRequest("POST", "/api/locations/", bytes.NewReader(data))
		router.ServeHTTP(w, req)
		assert.Equal(t, 200, w.Code)
		fmt.Println(w.Body.String())
	})
	t.Run("test create invalid location", func(t *testing.T) {
		data, _ := json.Marshal(map[string]string{"query": "AAAAAAAAAAAAAAAA"})
		w := httptest.NewRecorder()
		req := httptest.NewRequest("POST", "/api/locations/", bytes.NewReader(data))
		router.ServeHTTP(w, req)
		assert.Equal(t, 400, w.Code)
		fmt.Println(w.Body.String())
	})

	t.Run("test update with no data", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("PATCH", locationURL, nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, 400, w.Code)
		fmt.Println(w.Body.String())
	})

	t.Run("test delete", func(t *testing.T) {
		w := httptest.NewRecorder()
		req := httptest.NewRequest("DELETE", locationURL, nil)
		router.ServeHTTP(w, req)
		assert.Equal(t, http.StatusNoContent, w.Code)
	})

}

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}
