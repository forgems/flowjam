package locations

import (
	"database/sql"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/forgems/flowjam/weather"
)

type sqlstore struct {
	db *sqlx.DB
}

func NewPGStore(name string) (Store, error) {
	db, err := sqlx.Open("postgres", name)
	if err != nil {
		return nil, err
	}
	return &sqlstore{db}, nil
}

func (s *sqlstore) DB() (*sqlx.DB, error) {
	return s.db, nil
}

func (s *sqlstore) List() ([]*Location, error) {
	db, err := s.DB()
	if err != nil {
		return nil, err
	}
	locations := []*Location{}
	err = db.Select(&locations, `SELECT id, query FROM locations`)
	return locations, err
}

func (s *sqlstore) Get(id int) (*Location, error) {
	db, err := s.DB()
	if err != nil {
		return nil, err
	}
	location := Location{}
	err = db.Get(&location, `SELECT id, query from locations where id = $1`, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return &location, err
}

func (s *sqlstore) Save(location *Location) error {
	db, err := s.DB()
	if err != nil {
		return err
	}
	if location.ID > 0 {
		db.MustExec(
			"UPDATE locations SET query = $1 WHERE id = $2",
			location.Query, location.ID,
		)
	} else {
		err := db.Get(&location.ID,
			"INSERT INTO locations(query) VALUES ($1) RETURNING id", location.Query,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *sqlstore) Delete(location *Location) error {
	db, err := s.DB()
	if err != nil {
		return err
	}
	_, err = db.Exec(
		"DELETE FROM locations WHERE id=$1", location.ID,
	)
	return err
}

func (s *sqlstore) GetStatistics(location *Location) (*LocationStatistics, error) {
	db, err := s.DB()
	if err != nil {
		return nil, err
	}

	stats := LocationStatistics{
		ConditionsDays:      map[string]int{},
		AverageTempsByMonth: map[string]float32{},
		MinTempsByMonth:     map[string]float32{},
		MaxTempsByMonth:     map[string]float32{},
	}
	err = db.Get(
		&stats.QueryCount,
		`SELECT
			COUNT(*)
		FROM
			queries
		WHERE
			location_id=$1`,
		location.ID,
	)
	if err != nil {
		return nil, err
	}
	buf := struct {
		Date    time.Time `db:"date"`
		AvgTemp float32   `db:"avg_temp"`
		MinTemp float32   `db:"min_temp"`
		MaxTemp float32   `db:"max_temp"`
	}{}
	rows, err := db.Queryx(`SELECT
		date_trunc('month', timestamp) as date,
		AVG(temp) as avg_temp,
		MIN(min_temp) as min_temp,
		MAX(max_temp) as max_temp
	FROM
		queries
	WHERE queries.location_id = $1
	GROUP BY
		date
	ORDER BY
		date
	`, location.ID)

	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err := rows.StructScan(&buf)
		if err != nil {
			return nil, err
		}
		month := buf.Date.Format("2006-01")
		stats.AverageTempsByMonth[month] = buf.AvgTemp
		stats.MaxTempsByMonth[month] = buf.MaxTemp
		stats.MinTempsByMonth[month] = buf.MinTemp
	}

	rows, err = db.Queryx(
		`SELECT
			lc.name as name, COUNT(DISTINCT date_trunc('day', q.timestamp)) AS count
		FROM
			location_conditions lc JOIN queries q ON lc.query_id = q.id
		WHERE
			q.location_id = $1
		GROUP BY
			name`, location.ID)

	if err != nil {
		return nil, err
	}

	condBuf := struct {
		Name  string
		Count int
	}{}

	for rows.Next() {
		err := rows.StructScan(&condBuf)
		if err != nil {
			return nil, err
		}
		stats.ConditionsDays[condBuf.Name] = condBuf.Count
	}
	return &stats, nil
}

func (s *sqlstore) SaveQuery(location *Location, data *weather.WeatherData) error {
	db, err := s.DB()
	if err != nil {
		return err
	}
	var queryId int
	err = db.Get(&queryId,
		`INSERT INTO
			queries(location_id, timestamp, temp, min_temp, max_temp)
		VALUES
			($1, $2, $3, $4, $5)
		RETURNING
			id`,
		location.ID, data.Time(), data.Temp(), data.TempMin(), data.TempMax(),
	)

	if err != nil {
		return err
	}

	for _, condition := range data.Conditions() {
		_, err := db.Exec(
			`INSERT INTO
				location_conditions(query_id, name)
			VALUES
				($1, $2)`,
			queryId, condition,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

var _ Store = &sqlstore{}
