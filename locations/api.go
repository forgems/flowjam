package locations

import (
	"net/http"
	"strconv"

	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/forgems/flowjam/weather"
)

type LocationHandler struct {
	Store   Store
	Weather weather.Service
}

func (h *LocationHandler) RegisterRoutes(router gin.IRouter) {
	router.GET("/api/locations/", h.List)
	router.POST("/api/locations/", h.Create)
	router.PUT("/api/locations/:id/", h.Update)
	router.PATCH("/api/locations/:id/", h.Update)
	router.DELETE("/api/locations/:id/", h.Delete)
	router.GET("/api/locations/:id/", h.GetWeather)
	router.GET("/api/locations/:id/statistics/", h.GetStatistics)
}

func (h *LocationHandler) List(c *gin.Context) {
	locations, err := h.Store.List()
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(500)
		return
	}
	c.JSON(200, locations)
}

type UpdateLocation struct {
	Query string `json:"query" binding:"required"`
}

func (h *LocationHandler) LocationId(c *gin.Context) int {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusNotFound)
		return -1
	}
	return id
}
func (h *LocationHandler) Create(c *gin.Context) {
	update := UpdateLocation{}
	err := c.BindJSON(&update)
	if err != nil {
		log.Println(err)
		return
	}
	_, err = h.Weather.CurrentWeather(update.Query)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]string{"message": err.Error()})
		return
	}

	location := Location{Query: update.Query}
	err = h.Store.Save(&location)
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, location)
}

func (h *LocationHandler) Update(c *gin.Context) {
	location, _ := h.Store.Get(h.LocationId(c))
	if location == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	update := UpdateLocation{}
	err := c.BindJSON(&update)
	if err != nil {
		log.Println(err)
		return
	}
	_, err = h.Weather.CurrentWeather(update.Query)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, map[string]string{"message": err.Error()})
		return
	}
	location.Query = update.Query
	err = h.Store.Save(location)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, location)
}

func (h *LocationHandler) Delete(c *gin.Context) {
	location, _ := h.Store.Get(h.LocationId(c))
	if location == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}
	err := h.Store.Delete(location)
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.Status(http.StatusNoContent)
}

func (h *LocationHandler) GetWeather(c *gin.Context) {
	location, err := h.Store.Get(h.LocationId(c))
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if location == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	current, err := h.Weather.CurrentWeather(location.Query)
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	err = h.Store.SaveQuery(location, current)
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	//convert Kelvins to Celsius
	current.Main.Temp = current.Temp()
	current.Main.TempMin = current.TempMin()
	current.Main.TempMax = current.TempMax()

	data := struct {
		*Location
		CurrentWeather *weather.WeatherData
	}{location, current}
	c.JSON(http.StatusOK, data)
}

func (h *LocationHandler) GetStatistics(c *gin.Context) {
	location, err := h.Store.Get(h.LocationId(c))
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	if location == nil {
		c.AbortWithStatus(http.StatusNotFound)
		return
	}

	stats, err := h.Store.GetStatistics(location)
	if err != nil {
		log.Println(err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.JSON(http.StatusOK, stats)
}
